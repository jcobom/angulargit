export class Author {
        public id: number;
        public name: string;
        public state: string;
        public avatar_url: string;
        public web_url: string;
}
