import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GitApiService } from 'src/app/services/git-api.service';
import { Issue } from 'src/app/classes/model/issue';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  public tarea: Issue=new Issue;
  public id : string;

  constructor(public _servicio:GitApiService,public activoR : ActivatedRoute, private _location: Location) { 
    
    //this._servicio.getTarea(this.id);
    //console.log(" El ide que pasamos es " + this.id);
    //console.log(" Devuelve " + this._servicio.getTarea(this.id));
    
  }

  ngOnInit() {
    this.id = this.activoR.snapshot.paramMap.get("id");
    console.log("LLEGAMOS");
    this._servicio.getTarea(this.id)
    .subscribe(
      result => {
        this.tarea = result;
        console.log(" En el init tenemos " + JSON.stringify(this.tarea));
      },
      error => {
        if (error !== null) {
          console.log("Error al obtener tarea: ");
          console.log(error);
          // this.mensajeError = error.msg;
        } 
      }
    );            
  }

  borrarDetalle(){
        this._servicio.borrarTarea(this.tarea.iid).subscribe(
        result => {
          console.log('Borrado el registro ' + this.tarea.iid);
          this._location.back();
        }
      );
  }
}
