import { ActivatedRoute } from '@angular/router';
import { GitApiService } from './../../services/git-api.service';
import { Component, OnInit } from '@angular/core';
import { Issue } from 'src/app/classes/model/issue';
import {Location} from '@angular/common';

@Component({
  selector: 'app-new-tarea',
  templateUrl: './new-tarea.component.html',
  styleUrls: ['./new-tarea.component.css']
})
export class NewTareaComponent implements OnInit {
  tarea = new Issue;
  id : string;
  modo : string;
  titulo : string;

  constructor(public _servicio:GitApiService,public activoR : ActivatedRoute, private _location: Location) { 
    this.id = this.activoR.snapshot.paramMap.get("id");
    if( this.id == null || this.id == "" ){
      this.modo = 'NUEVO';
      this.titulo = 'Nueva Incidencia';
    }else{
      this.modo = 'EDITAR';
      this._servicio.getTarea(this.id)
    .subscribe(
      result => {
        this.tarea = result;
        console.log(" En el init tenemos " + JSON.stringify(this.tarea));
      },
      error => {
        if (error !== null) {
          console.log("Error al obtener tarea: ");
          console.log(error);
          // this.mensajeError = error.msg;
        } 
      }
    );  
      this.titulo = 'Detalle de la Incidencia ';
    }
  }

  ngOnInit() {
   
    
  }


  guardar(){
    if ( this.modo == 'EDITAR'){
      console.log('Antes' );
      console.log(this.tarea);
      console.log('despues' );
      this._servicio.editarTarea(this.tarea)
      .subscribe(
        result =>{
          console.log(result); 
          this._location.back();
        },
        error =>{
          console.log(error);
        }
      )
    }else{
        this._servicio.newTarea(this.tarea)
      .subscribe(
        result =>{
          console.log(result); 
          this._location.back();
        },
        error =>{
          console.log(error);
        }
      )
    }    
  }
}
