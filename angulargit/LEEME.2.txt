
URL

https://gitlab.com/api/v4/projects/9959053/issues

Formato de respuesta
[
    {
        "id": 16715039,
        "iid": 1,
        "project_id": 9959053,
        "title": "Incidencia de prueba",
        "description": "Primera incidencia a listar\n\nTodo erróneo.",
        "state": "opened",
        "created_at": "2018-12-17T09:37:01.170Z",
        "updated_at": "2018-12-17T09:37:01.170Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [],
        "milestone": null,
        "assignees": [],
        "author": {
            "id": 3286080,
            "name": "Manu Caballero",
            "username": "manu.caballero",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/2192fc006e5c710af8900e3b881efbfc?s=80&d=identicon",
            "web_url": "https://gitlab.com/manu.caballero"
        },
        "assignee": null,
        "user_notes_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/jcobo1/angulargit/issues/1",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "weight": null
    }
]